public class Test_1{
    private int testNumber;
    private String testName;
    
    public void setTestNumber(int testNumber){
        this.testNumber=testNumber;
    }
    
    public void setTestName(String testName){
        this.testName=testName;
    }
    
    public int getTestNumber(){
        return testNumber;
    }
    
    public String getTestName(){
        return testName;
    }
}